package main

import (
  "bufio"
	"flag"
	"fmt"
	"log"
  "os"
  "strings"

	"github.com/coreos/pkg/flagutil"
	"github.com/dghubble/go-twitter/twitter"
	"github.com/dghubble/oauth1"
)

func main() {
	flags := struct {
		consumerKey    string
		consumerSecret string
		accessToken    string
		accessSecret   string
	}{}

  var (
    textInput string
    fileInput string
    textToTweet string
  )

	flag.StringVar(&flags.consumerKey, "consumer-key", "", "Twitter Consumer Key")
	flag.StringVar(&flags.consumerSecret, "consumer-secret", "", "Twitter Consumer Secret")
	flag.StringVar(&flags.accessToken, "access-token", "", "Twitter Access Token")
	flag.StringVar(&flags.accessSecret, "access-token-secret", "", "Twitter Access Token Secret")
  flag.StringVar(&textInput, "text", "", "Single line text to tweet")
  flag.StringVar(&fileInput, "path", "", "Path to file containing tweet text")

  flag.Parse()
  flagutil.SetFlagsFromEnv(flag.CommandLine, "TWITTER")

  if textInput == "" && fileInput == "" {
    log.Fatal("No content in tweet text or tweet file")
  } else {
    if textInput != "" {
      textToTweet = textInput
    } else {
      if fileInput != "" {
        file, err := os.Open(fileInput)
        if err != nil {
          log.Fatalf("failed opening file: %s", err)
        }
        scanner := bufio.NewScanner(file)
        scanner.Split(bufio.ScanLines)
        var txtlines []string

        for scanner.Scan() {
          txtlines = append(txtlines, scanner.Text())
        }
        file.Close()
        textToTweet = strings.Join(txtlines, "\n")
      }
    }
  }

  if len(textToTweet) < 3 {
    log.Fatal("Short content in tweet text or tweet file")
  }

  if len(textToTweet) > 180 {
    log.Fatal("Excessively long content in tweet text or tweet file")
  }

	if flags.consumerKey == "" {
		log.Fatal("Application Consumer Key required")
	}

  if flags.consumerSecret == "" {
    log.Fatal("Application Consumer Secret]")
  }

  if flags.accessToken == "" {
    log.Fatal("Application Access Token required")
  }

  if flags.accessSecret == "" {
    log.Fatal("Application Access Token secret required")
  }


	config := oauth1.NewConfig(flags.consumerKey, flags.consumerSecret)
	token := oauth1.NewToken(flags.accessToken, flags.accessSecret)
	httpClient := config.Client(oauth1.NoContext, token)
	client := twitter.NewClient(httpClient)

  // replaced "resp" with _ since it's not being parsed.
	tweet, _, err := client.Statuses.Update(textToTweet, nil)

  fmt.Println("id = " + tweet.IDStr)
  if err != nil {
    fmt.Println("error  found")
    fmt.Println(err)
    os.Exit(2)
  }
}