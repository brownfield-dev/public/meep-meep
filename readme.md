# Runner + Tweet = Meep Meep

This image stems from a desire to have a pipeline that runs whenever I add a blog post to my www-brownfield-dev project. 
I made that project private so I can be sloppy with customer names in the planning and drafting stages without creating 
trouble for myself, however I wanted to share this capability with the world.

The container has a script that uses shell functions to authenticate against twitter and send the contents of the argument
as a tweet. 

User credentials come from environment variables.  Be sure to set all of the following in project settings > CI Variables. 

```plaintext
TWITTER_ACCESS_TOKEN
TWITTER_ACCESS_TOKEN_SECRET
TWITTER_CONSUMER_KEY
TWITTER_CONSUMER_SECRET
BLOG_URL_STUB
```

`BLOG_URL_STUB` should be something like `https://brownfield.dev/` in my case 
since the `...CHANGED_PAGE_PATHS` includes `posts/filename.html`. 

Get all of those `TWITTER_...` variables from going to twitter and creating a "new 
application" and setting it as a "bot". 

```yaml
stages:
  - build
  - review
  - deploy
  - tweet

review:
  image: alpine
  stage: review
  script:
    - echo "${CI_COMMIT_MESSAGE}" > tweet.txt
    - echo "${BLOG_URL_STUB}${CI_MERGE_REQUEST_CHANGED_PAGE_PATHS}" >> tweet.txt
  only:
    - branches
  except:
    - master
  environment:
    name: local
    url: http://127.0.0.1:1313
  artifacts:
    expose_as: 'Tweet preview'
    paths: ['tweet.txt']
  variables:
    GIT_STRATEGY: none

tweet:
  stage: tweet
  image: registry.gitlab.com/brownfield-dev/public/meep-meep:v2-0
  script:
    - meep -path tweet.txt
  only:
    - branches
  except:
    - master
  when: manual
  environment:
    name: twitter
    url: 'https://twitter.com/{{USERNAME}}?x=#'
  dependencies:
    - review
  variables:
    GIT_STRATEGY: none
```

The Preview Tweet job above generates the text to be tweeted.   

The tweet job depends on the "review" job so that it will fail if it can't get 
the `tweet.txt` artifact.  

The twitter environment URL needs to discard the modified file path which would 
normally be added to the end.  Using the # lets twitter throw it away as a 
missing anchor.
